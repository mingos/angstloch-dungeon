const Cell = require("angstloch").Cell;

const TILES = {};
TILES[Cell.TYPE_FLOOR] = " ";
TILES[Cell.TYPE_WALL] = "#";

module.exports.mapToArray = function(map) {
	let result = [];
	for (let y = 0; y < map.height; ++y) {
		result[y] = "";
		for (let x = 0; x < map.width; ++x) {
			result[y] += TILES[map.getCellType(x, y)];
		}
	}

	return result;
};
