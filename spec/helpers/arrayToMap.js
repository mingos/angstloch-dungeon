const Map = require("angstloch").Map;
const Cell = require("angstloch").Cell;

const TILES = {};
TILES[" "] = Cell.TYPE_FLOOR;
TILES["#"] = Cell.TYPE_WALL;

module.exports.arrayToMap = function(rows) {
	let map = new Map();
	map.init(rows[0].length, rows.length);

	for (let y = 0; y < rows.length; ++y) {
		let row = rows[y].split("");
		for (let x = 0; x < row.length; ++x) {
			map.setCellType(x, y, TILES[row[x]]);
		}
	}

	return map;
};
