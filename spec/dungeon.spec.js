const expect = require("chai").expect;
const arrayToMap = require("./helpers/arrayToMap").arrayToMap;
const mapToArray = require("./helpers/mapToArray").mapToArray;
const Dungeon = require("../lib/dungeon").Dungeon;
const Point = require("angstloch").Point;

describe("Dungeon", () => {
	let dungeon;

	beforeEach(() => {
		dungeon = new Dungeon();
	});

	it("should get potential room placement points", () => {
		// given
		let map = arrayToMap([
			"#######",
			"#   ###",
			"#   ###",
			"#   ###",
			"#######",
			"#######",
			"#######"
		]);
		let expectedPositions = [
			new Point(5, 1),
			new Point(5, 3),
			new Point(1, 5),
			new Point(3, 5),
			new Point(5, 5)
		];
		dungeon._map = map;

		// when
		let positions = dungeon._getPotentialRoomPlacementPositions();

		// then
		expect(positions).to.deep.equal(expectedPositions);
	});
});
