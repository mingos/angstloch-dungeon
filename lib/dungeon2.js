"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var angstloch_1 = require("angstloch");
var room2_1 = require("./room2");
var Dungeon2 = (function () {
    function Dungeon2() {
    }
    Dungeon2.prototype._getOptions = function (options) {
        options = options || {};
        var output = {
            width: 80,
            height: 25
        };
        for (var p in options) {
            if (options.hasOwnProperty(p)) {
                output[p] = options[p];
            }
        }
        return output;
    };
    Dungeon2.prototype.generate = function (options) {
        options = this._getOptions(options);
        this._map = new angstloch_1.Map(options.width, options.height);
        for (var x = 0; x < options.width; ++x) {
            for (var y = 0; y < options.height; ++y) {
                this._map.setCellType(x, y, angstloch_1.Cell.TYPE_WALL);
            }
        }
        var room1 = this._placeRoom();
        var room2 = this._placeRoom();
        console.log(room1, this._getRoomExitPosition(room1));
        console.log(room2, this._getRoomExitPosition(room2));
        return this._map;
    };
    Dungeon2.prototype._placeRoom = function () {
        var potentialPositions = this._getPotentialRoomPlacementPositions();
        var room = new room2_1.Room2(new angstloch_1.Point(angstloch_1.Random.odd(3, 9), angstloch_1.Random.odd(3, 9)));
        while (potentialPositions.length) {
            room.setPosition(angstloch_1.Random.listElement(potentialPositions));
            console.log(potentialPositions.length, potentialPositions.indexOf(room.topLeft));
            if (!this._canRoomBePlacedOnMap(room)) {
                potentialPositions.splice(potentialPositions.indexOf(room.topLeft), 1);
                continue;
            }
            room.digSelfOnMap(this._map);
            console.log("room: ", room.topLeft, room.bottomRight, room.dimensions);
            return room;
        }
        return null;
    };
    Dungeon2.prototype._canRoomBePlacedOnMap = function (room) {
        if (room.bottomRight.x >= this._map.width - 1 || room.bottomRight.y >= this._map.height - 1) {
            return false;
        }
        for (var x = room.topLeft.x; x <= room.bottomRight.x; x += 2) {
            for (var y = room.topLeft.y; y <= room.bottomRight.y; y += 2) {
                if (this._map.getCellType(x, y) === angstloch_1.Cell.TYPE_FLOOR) {
                    return false;
                }
            }
        }
        return true;
    };
    Dungeon2.prototype._getPotentialRoomPlacementPositions = function () {
        var positions = [];
        for (var y = 1; y < this._map.height - 1; y += 2) {
            for (var x = 1; x < this._map.width - 1; x += 2) {
                var cellType = this._map.getCellType(x, y);
                if (cellType === angstloch_1.Cell.TYPE_WALL || cellType === angstloch_1.Cell.TYPE_UNDEFINED) {
                    positions.push(new angstloch_1.Point(x, y));
                }
            }
        }
        return positions;
    };
    Dungeon2.prototype._getRoomExitPosition = function (room) {
        var exitPosition = room.getRandomExitPosition();
        while (!this._map.containsInBounds(exitPosition)) {
            exitPosition = room.getRandomExitPosition();
        }
        return exitPosition;
    };
    Dungeon2.prototype._digCorridorBetween = function (start, end) {
    };
    return Dungeon2;
}());
exports.Dungeon2 = Dungeon2;
