"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var angstloch_1 = require("angstloch");
var Room2 = (function () {
    function Room2(dimensions) {
        this._dimensions = dimensions;
        this._map = new angstloch_1.Map(dimensions.x, dimensions.y);
        for (var y = 0; y < this._map.height; ++y) {
            for (var x = 0; x < this._map.width; ++x) {
                this._map.setCellType(x, y, angstloch_1.Cell.TYPE_FLOOR);
            }
        }
    }
    Object.defineProperty(Room2.prototype, "dimensions", {
        get: function () {
            return this._dimensions;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Room2.prototype, "topLeft", {
        get: function () {
            return this._topLeft;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Room2.prototype, "bottomRight", {
        get: function () {
            return this._bottomRight;
        },
        enumerable: true,
        configurable: true
    });
    Room2.prototype.setPosition = function (position) {
        this._topLeft = position;
        this._bottomRight = this.topLeft.translate(this.dimensions).translate(-1, -1);
    };
    Room2.prototype.digSelfOnMap = function (map) {
        var _this = this;
        this._map.forEachCell(function (x, y, cell) {
            map.setCellType(_this.topLeft.translate(x, y), cell.type);
        });
    };
    Room2.prototype.getRandomExitPosition = function () {
        var x;
        var y;
        if (angstloch_1.Random.float() > 0.5) {
            x = angstloch_1.Random.float() > 0.5 ? this.topLeft.x - 2 : this.bottomRight.x + 2;
            y = angstloch_1.Random.odd(this.topLeft.y, this.bottomRight.y);
        }
        else {
            y = angstloch_1.Random.float() > 0.5 ? this.topLeft.y - 2 : this.bottomRight.y + 2;
            x = angstloch_1.Random.odd(this.topLeft.x, this.bottomRight.x);
        }
        return new angstloch_1.Point(x, y);
    };
    return Room2;
}());
exports.Room2 = Room2;
