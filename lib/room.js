"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var angstloch_1 = require("angstloch");
var Room = (function () {
    function Room(dimensions, topLeft, bottomRight) {
        this.dimensions = dimensions;
        this.topLeft = topLeft;
        this.bottomRight = bottomRight;
        this.exits = [];
        this.potentialExits = [];
        this.map = new angstloch_1.Map(dimensions.x, dimensions.y);
        for (var y = 0; y < this.map.height; ++y) {
            for (var x = 0; x < this.map.width; ++x) {
                this.map.setCellType(x, y, angstloch_1.Cell.TYPE_FLOOR);
            }
        }
    }
    return Room;
}());
exports.Room = Room;
