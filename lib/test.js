const Dungeon = require("./index").Dungeon;
const Dungeon2 = require("./index").Dungeon2;
const Cell = require("angstloch").Cell;
const colors = require("colors");
const path = require("path");

const TILES = {};
TILES[Cell.TYPE_FLOOR] = " ";
TILES[Cell.TYPE_WALL] = "#".grey;
TILES[Cell.TYPE_DOOR] = "+".red;
TILES[Cell.TYPE_UNDEFINED] = "?";

const angstlochDungeon = new Dungeon2();
const dungeon = angstlochDungeon.generate({
	width: 80,
	height: 25
});

const display = [];

dungeon.forEachCell((x, y, cell) => {
	display[y] = display[y] || [];
	display[y][x] = TILES[cell.type];
});

// console.log("options:".green)
// console.log(JSON.stringify(angstlochDungeon._options, null, "    ") + "\n");

display.forEach(row => {
	console.log(row.join(""));
});
