"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var angstloch_1 = require("angstloch");
var angstloch_maze_1 = require("angstloch-maze");
var display_map_1 = require("./display-map");
var room_1 = require("./room");
var Dungeon = (function () {
    function Dungeon() {
        this._map = new angstloch_1.Map();
    }
    Dungeon.prototype.generate = function (options) {
        var _this = this;
        this._map.init(options.width, options.height);
        for (var x = 0; x < options.width; ++x) {
            for (var y = 0; y < options.height; ++y) {
                this._map.setCellType(x, y, angstloch_1.Cell.TYPE_WALL);
            }
        }
        var rooms = this._placeRooms();
        display_map_1.displayMap(this._map);
        console.log(JSON.stringify(rooms[0], null, "\t"));
        var maze = new angstloch_maze_1.Maze();
        var emptyPoints = this._getPotentialRoomPlacementPositions();
        emptyPoints.forEach(function (point) {
            if (_this._map.getCellType(point) === angstloch_1.Cell.TYPE_WALL) {
                maze.generate({
                    map: _this._map,
                    loopChance: 0.25,
                    startX: point.x,
                    startY: point.y,
                    straight: 0.5
                });
            }
        });
        rooms.forEach(function (room) {
            _this._addDoorToRoom(room);
        });
        for (var y = 1; y < this._map.height - 1; y += 2) {
            for (var x = 1; x < this._map.width - 1; x += 2) {
                var position = new angstloch_1.Point(x, y);
                if (this._isDeadEnd(position)) {
                    this._removeDeadEnd(position);
                }
            }
        }
        return this._map;
    };
    Dungeon.prototype._placeRooms = function () {
        var maxRooms = 10;
        var rooms = [];
        for (var i = 0; i < maxRooms; ++i) {
            var room = this._placeRoom();
            if (!room) {
                break;
            }
            rooms.push(room);
        }
        return rooms;
    };
    Dungeon.prototype._placeRoom = function () {
        var potentialPositions = this._getPotentialRoomPlacementPositions();
        var dimensions = new angstloch_1.Point(angstloch_1.Random.odd(3, 9), angstloch_1.Random.odd(3, 9));
        outer: while (potentialPositions.length) {
            var topLeft = potentialPositions[angstloch_1.Random.int(0, potentialPositions.length - 1)];
            if (this._map.getCellType(topLeft) === angstloch_1.Cell.TYPE_FLOOR) {
                potentialPositions.splice(potentialPositions.indexOf(topLeft), 1);
                continue;
            }
            var bottomRight = topLeft.translate(dimensions).translate(-1, -1);
            if (bottomRight.x >= this._map.width - 1 || bottomRight.y >= this._map.height - 1) {
                potentialPositions.splice(potentialPositions.indexOf(topLeft), 1);
                continue;
            }
            for (var x = topLeft.x; x <= bottomRight.x; x += 2) {
                for (var y = topLeft.y; y <= bottomRight.y; y += 2) {
                    if (this._map.getCellType(x, y) === angstloch_1.Cell.TYPE_FLOOR) {
                        potentialPositions.splice(potentialPositions.indexOf(topLeft), 1);
                        continue outer;
                    }
                }
            }
            for (var x = topLeft.x; x <= bottomRight.x; ++x) {
                for (var y = topLeft.y; y <= bottomRight.y; ++y) {
                    this._map.setCellType(x, y, angstloch_1.Cell.TYPE_FLOOR);
                }
            }
            return new room_1.Room(dimensions, topLeft, bottomRight);
        }
        return null;
    };
    Dungeon.prototype._getPotentialRoomPlacementPositions = function () {
        var positions = [];
        for (var y = 1; y < this._map.height - 1; y += 2) {
            for (var x = 1; x < this._map.width - 1; x += 2) {
                if (this._map.getCellType(x, y) === angstloch_1.Cell.TYPE_WALL) {
                    positions.push(new angstloch_1.Point(x, y));
                }
            }
        }
        return positions;
    };
    Dungeon.prototype._addDoorToRoom = function (room) {
        var _this = this;
        var x;
        var y;
        var offset;
        var possibleOffsets = angstloch_1.Offsets.filter(function (offset) {
            var offset1 = room.topLeft.translate(offset).translate(offset);
            var offset2 = room.bottomRight.translate(offset).translate(offset);
            return offset1.x > 0 && offset1.x < _this._map.width - 1 &&
                offset2.x > 0 && offset2.x < _this._map.width - 1 &&
                offset1.y > 0 && offset1.y < _this._map.height - 1 &&
                offset2.y > 0 && offset2.y < _this._map.height - 1;
        });
        offset = possibleOffsets[angstloch_1.Random.int(0, possibleOffsets.length - 1)];
        if (offset.x) {
            x = offset.x < 0 ? room.topLeft.x : room.bottomRight.x;
            y = angstloch_1.Random.odd(room.topLeft.y, room.bottomRight.y);
        }
        else {
            y = offset.y < 0 ? room.topLeft.y : room.bottomRight.y;
            x = angstloch_1.Random.odd(room.topLeft.x, room.bottomRight.x);
        }
        this._map.setCellType(new angstloch_1.Point(x, y).translate(offset), angstloch_1.Cell.TYPE_DOOR);
    };
    Dungeon.prototype._isDeadEnd = function (position) {
        var _this = this;
        if (this._map.getCellType(position) !== angstloch_1.Cell.TYPE_FLOOR) {
            return false;
        }
        return angstloch_1.Offsets
            .map(function (offset) { return _this._map.getCellType(position.translate(offset)); })
            .filter(function (type) { return type === angstloch_1.Cell.TYPE_WALL; })
            .length >= 3;
    };
    Dungeon.prototype._removeDeadEnd = function (position) {
        var _this = this;
        var direction = angstloch_1.Offsets
            .filter(function (offset) {
            return _this._map.getCellType(position.translate(offset)) === angstloch_1.Cell.TYPE_FLOOR;
        })[0];
        this._map.setCellType(position, angstloch_1.Cell.TYPE_WALL);
        if (direction) {
            this._map.setCellType(position.translate(direction), angstloch_1.Cell.TYPE_WALL);
            var nextDeadEndCandidate = position.translate(direction).translate(direction);
            if (this._isDeadEnd(nextDeadEndCandidate)) {
                this._removeDeadEnd(nextDeadEndCandidate);
            }
        }
    };
    return Dungeon;
}());
exports.Dungeon = Dungeon;
