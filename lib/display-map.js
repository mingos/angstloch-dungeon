"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var angstloch_1 = require("angstloch");
var colors = require("colors");
function displayMap(map) {
    var TILES = {};
    TILES[angstloch_1.Cell.TYPE_FLOOR] = " ";
    TILES[angstloch_1.Cell.TYPE_WALL] = colors.grey("#");
    TILES[angstloch_1.Cell.TYPE_DOOR] = colors.red("+");
    TILES[angstloch_1.Cell.TYPE_UNDEFINED] = "?";
    var display = [];
    map.forEachCell(function (x, y, cell) {
        display[y] = display[y] || [];
        display[y][x] = TILES[cell.type];
    });
    display.forEach(function (row) {
        console.log(row.join(""));
    });
}
exports.displayMap = displayMap;
