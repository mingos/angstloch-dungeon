import { IOptions } from "./i-options";
import { Map } from "angstloch";
export declare class Dungeon2 {
    private _map;
    private _getOptions(options?);
    generate(options?: IOptions): Map;
    private _placeRoom();
    private _canRoomBePlacedOnMap(room);
    private _getPotentialRoomPlacementPositions();
    private _getRoomExitPosition(room);
    private _digCorridorBetween(start, end);
}
