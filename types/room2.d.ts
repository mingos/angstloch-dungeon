import { Point, Map } from "angstloch";
export declare class Room2 {
    private _dimensions;
    private _topLeft;
    private _bottomRight;
    private _map;
    constructor(dimensions: Point);
    readonly dimensions: Point;
    readonly topLeft: Point;
    readonly bottomRight: Point;
    setPosition(position: Point): void;
    digSelfOnMap(map: Map): void;
    getRandomExitPosition(): Point;
}
