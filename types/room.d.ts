import { Point, Map } from "angstloch";
export declare class Room {
    dimensions: Point;
    topLeft: Point;
    bottomRight: Point;
    exits: Array<Point>;
    potentialExits: Array<Point>;
    map: Map;
    constructor(dimensions: Point, topLeft: Point, bottomRight: Point);
}
