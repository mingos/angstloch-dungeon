import { Map } from "angstloch";
import { IOptions } from "./i-options";
export declare class Dungeon {
    private _map;
    constructor();
    generate(options: IOptions): Map;
    private _placeRooms();
    private _placeRoom();
    private _getPotentialRoomPlacementPositions();
    private _addDoorToRoom(room);
    private _isDeadEnd(position);
    private _removeDeadEnd(position);
}
