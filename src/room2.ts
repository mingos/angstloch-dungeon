import {Point, Cell, Map, Random} from "angstloch";

export class Room2 {
	private _dimensions: Point;
	private _topLeft: Point;
	private _bottomRight: Point;
	private _map: Map;

	constructor(dimensions: Point) {
		this._dimensions = dimensions;
		this._map = new Map(dimensions.x, dimensions.y);
		for (let y = 0; y < this._map.height; ++y) {
			for (let x = 0; x < this._map.width; ++x) {
				this._map.setCellType(x, y, Cell.TYPE_FLOOR);
			}
		}
	}

	get dimensions(): Point {
		return this._dimensions;
	}

	get topLeft(): Point {
		return this._topLeft;
	}

	get bottomRight(): Point {
		return this._bottomRight;
	}

	public setPosition(position: Point): void {
		this._topLeft = position;
		this._bottomRight = this.topLeft.translate(this.dimensions).translate(-1, -1);
	}

	public digSelfOnMap(map: Map): void {
		this._map.forEachCell((x, y, cell) => {
			map.setCellType(this.topLeft.translate(x, y), cell.type);
		});
	}

	public getRandomExitPosition(): Point {
		let x: number;
		let y: number;
		if (Random.float() > 0.5) {
			x = Random.float() > 0.5 ? this.topLeft.x - 2 : this.bottomRight.x + 2;
			y = Random.odd(this.topLeft.y, this.bottomRight.y);
		} else {
			y = Random.float() > 0.5 ? this.topLeft.y - 2 : this.bottomRight.y + 2;
			x = Random.odd(this.topLeft.x, this.bottomRight.x);
		}

		return new Point(x, y);
	}
}
