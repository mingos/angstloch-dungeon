import {IOptions} from "./i-options";
import {Map, Cell, Point, Random} from "angstloch";
import {Room2} from "./room2";

export class Dungeon2 {
	private _map: Map;

	private _getOptions(options?: IOptions): IOptions {
		options = options || {};
		let output = {
			width: 80,
			height: 25
		};
		for (let p in options) {
			if (options.hasOwnProperty(p)) {
				output[p] = options[p];
			}
		}
		return output;
	}

	public generate(options?: IOptions): Map {
		options = this._getOptions(options);

		this._map = new Map(options.width, options.height);
		for (let x = 0; x < options.width; ++x) {
			for (let y = 0; y < options.height; ++y) {
				this._map.setCellType(x, y, Cell.TYPE_WALL);
			}
		}

		let room1 = this._placeRoom();
		let room2 = this._placeRoom();

		console.log(room1, this._getRoomExitPosition(room1));
		console.log(room2, this._getRoomExitPosition(room2));

		return this._map;
	}

	private _placeRoom(): Room2 {
		let potentialPositions: Array<Point> = this._getPotentialRoomPlacementPositions();

		// TODO configurable room dimensions
		let room: Room2 = new Room2(new Point(Random.odd(3, 9), Random.odd(3, 9)));

		while (potentialPositions.length) {
			room.setPosition(Random.listElement(potentialPositions));
			console.log(potentialPositions.length, potentialPositions.indexOf(room.topLeft));

			if (!this._canRoomBePlacedOnMap(room)) {
				// remove the position from the list
				potentialPositions.splice(potentialPositions.indexOf(room.topLeft), 1);
				continue;
			}

			room.digSelfOnMap(this._map);

			console.log("room: ", room.topLeft, room.bottomRight, room.dimensions);

			return room;
		}

		return null;
	}

	private _canRoomBePlacedOnMap(room: Room2): boolean {
		// map dimensions:
		if (room.bottomRight.x >= this._map.width - 1 || room.bottomRight.y >= this._map.height - 1) {
			return false;
		}

		// overlapping cells:
		for (let x = room.topLeft.x; x <= room.bottomRight.x; x += 2) {
			for (let y = room.topLeft.y; y <= room.bottomRight.y; y += 2) {
				if (this._map.getCellType(x, y) === Cell.TYPE_FLOOR) {
					return false;
				}
			}
		}

		return true;
	}

	private _getPotentialRoomPlacementPositions(): Array<Point> {
		let positions: Array<Point> = [];
		for (let y = 1; y < this._map.height - 1; y += 2) {
			for (let x = 1; x < this._map.width - 1; x += 2) {
				let cellType = this._map.getCellType(x, y);
				if (cellType === Cell.TYPE_WALL || cellType === Cell.TYPE_UNDEFINED) {
					positions.push(new Point(x, y));
				}
			}
		}

		return positions;
	}

	private _getRoomExitPosition(room: Room2): Point {
		let exitPosition = room.getRandomExitPosition();
		while (!this._map.containsInBounds(exitPosition)) {
			exitPosition = room.getRandomExitPosition();
		}

		return exitPosition;
	}

	private _digCorridorBetween(start: Point, end: Point): void {
		// TODO come up with the correct algorithm
	}
}
