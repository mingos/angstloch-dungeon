import {Cell, Map} from "angstloch";
import * as colors from "colors";

export function displayMap(map: Map): void {
	const TILES = {};
	TILES[Cell.TYPE_FLOOR] = " ";
	TILES[Cell.TYPE_WALL] = colors.grey("#");
	TILES[Cell.TYPE_DOOR] = colors.red("+");
	TILES[Cell.TYPE_UNDEFINED] = "?";

	const display = [];

	map.forEachCell((x, y, cell) => {
		display[y] = display[y] || [];
		display[y][x] = TILES[cell.type];
	});

	display.forEach(row => {
		console.log(row.join(""));
	});

}
