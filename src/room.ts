import {Point, Cell, Map} from "angstloch";

export class Room {
	public dimensions: Point;
	public topLeft: Point;
	public bottomRight: Point;
	public exits: Array<Point>;
	public potentialExits: Array<Point>;
	public map: Map;

	constructor(dimensions: Point, topLeft: Point, bottomRight: Point) {
		this.dimensions = dimensions;
		this.topLeft = topLeft;
		this.bottomRight = bottomRight;
		this.exits = [];
		this.potentialExits = [];
		this.map = new Map(dimensions.x, dimensions.y);
		for (let y = 0; y < this.map.height; ++y) {
			for (let x = 0; x < this.map.width; ++x) {
				this.map.setCellType(x, y, Cell.TYPE_FLOOR);
			}
		}
	}
}
