import {Cell, Map, Offsets, Point, Random} from "angstloch";
import {IOptions} from "./i-options";
import {Maze} from "angstloch-maze";
import {displayMap} from "./display-map";
import {Room} from "./room";

export class Dungeon {
	private _map: Map;

	constructor() {
		this._map = new Map();
	}

	public generate(options: IOptions): Map {
		// TODO assign options to class property

		// prefill map with walls
		this._map.init(options.width, options.height);
		for (let x = 0; x < options.width; ++x) {
			for (let y = 0; y < options.height; ++y) {
				this._map.setCellType(x, y, Cell.TYPE_WALL);
			}
		}

		// place rooms
		let rooms: Array<Room> = this._placeRooms();
		displayMap(this._map);

		console.log(JSON.stringify(rooms[0], null, "\t"));

		// carve corridors
		const maze = new Maze();
		let emptyPoints = this._getPotentialRoomPlacementPositions();
		emptyPoints.forEach(point => {
			if (this._map.getCellType(point) === Cell.TYPE_WALL) {
				maze.generate({
					map: this._map,
					loopChance: 0.25,
					startX: point.x,
					startY: point.y,
					straight: 0.5
				});
				// displayMap(this._map);
			}
		});

		// add doors
		rooms.forEach(room => {
			this._addDoorToRoom(room);
		});
		// displayMap(this._map);

		// remove dead ends
		for (let y = 1; y < this._map.height - 1; y += 2) {
			for (let x = 1; x < this._map.width - 1; x += 2) {
				let position = new Point(x, y);
				if (this._isDeadEnd(position)) {
					this._removeDeadEnd(position);
				}
			}
		}

		return this._map;
	}

	private _placeRooms(): Array<Room> {
		// TODO configurable max number of rooms
		let maxRooms = 10;
		let rooms: Array<Room> = [];
		for (let i = 0; i < maxRooms; ++i) {
			let room: Room = this._placeRoom();
			if (!room) {
				break;
			}
			rooms.push(room);
		}

		return rooms;
	}

	private _placeRoom(): Room {
		let potentialPositions: Array<Point> = this._getPotentialRoomPlacementPositions();

		// TODO configurable room dimensions
		let dimensions: Point = new Point(Random.odd(3, 9), Random.odd(3, 9));

		outer: while (potentialPositions.length) {
			let topLeft: Point = potentialPositions[Random.int(0, potentialPositions.length - 1)];
			// 1. check if top left corner is a free position
			if (this._map.getCellType(topLeft) === Cell.TYPE_FLOOR) {
				// the position is already occupied - remove it from the list
				potentialPositions.splice(potentialPositions.indexOf(topLeft), 1);
				continue;
			}

			// OK, the top left corner is on an empty cell
			// 2. check if the room will fit the map
			let bottomRight = topLeft.translate(dimensions).translate(-1, -1);
			if (bottomRight.x >= this._map.width - 1 || bottomRight.y >= this._map.height - 1) {
				// it won't; let's try again
				potentialPositions.splice(potentialPositions.indexOf(topLeft), 1);
				continue;
			}

			// room dimensions fit in the map. Check if any other of the room's cells overlaps another room
			for (let x = topLeft.x; x <= bottomRight.x; x += 2) {
				for (let y = topLeft.y; y <= bottomRight.y; y += 2) {
					if (this._map.getCellType(x, y) === Cell.TYPE_FLOOR) {
						// room overlaps another room. Discard the position and retry.
						potentialPositions.splice(potentialPositions.indexOf(topLeft), 1);
						continue outer;
					}
				}
			}

			// room does not overlap anything; place it.
			for (let x = topLeft.x; x <= bottomRight.x; ++x) {
				for (let y = topLeft.y; y <= bottomRight.y; ++y) {
					this._map.setCellType(x, y, Cell.TYPE_FLOOR);
				}
			}

			return new Room(dimensions, topLeft, bottomRight);
		}

		return null;
	}

	private _getPotentialRoomPlacementPositions(): Array<Point> {
		let positions: Array<Point> = [];
		for (let y = 1; y < this._map.height - 1; y += 2) {
			for (let x = 1; x < this._map.width - 1; x += 2) {
				if (this._map.getCellType(x, y) === Cell.TYPE_WALL) {
					positions.push(new Point(x, y));
				}
			}
		}

		return positions;
	}

	private _addDoorToRoom(room: Room) {
		let x: number;
		let y: number;
		let offset: Point;

		let possibleOffsets = Offsets.filter(offset => {
			let offset1 = room.topLeft.translate(offset).translate(offset);
			let offset2 = room.bottomRight.translate(offset).translate(offset);
			return offset1.x > 0 && offset1.x < this._map.width - 1 &&
				offset2.x > 0 && offset2.x < this._map.width - 1 &&
				offset1.y > 0 && offset1.y < this._map.height - 1 &&
				offset2.y > 0 && offset2.y < this._map.height - 1;
		});
		offset = possibleOffsets[Random.int(0, possibleOffsets.length - 1)];

		if (offset.x) {
			x = offset.x < 0 ? room.topLeft.x : room.bottomRight.x;
			y = Random.odd(room.topLeft.y, room.bottomRight.y);
		} else {
			y = offset.y < 0 ? room.topLeft.y : room.bottomRight.y;
			x = Random.odd(room.topLeft.x, room.bottomRight.x);
		}

		this._map.setCellType(new Point(x, y).translate(offset), Cell.TYPE_DOOR);
	}

	private _isDeadEnd(position: Point): boolean {
		if (this._map.getCellType(position) !== Cell.TYPE_FLOOR) {
			return false;
		}

		return Offsets
			.map(offset => this._map.getCellType(position.translate(offset)))
			.filter(type => type === Cell.TYPE_WALL)
			.length >= 3;
	}

	private _removeDeadEnd(position): void {
		// find the next cell of the corridor
		let direction: Point = Offsets
			.filter(offset => {
				return this._map.getCellType(position.translate(offset)) === Cell.TYPE_FLOOR;
			})[0];

		// cave in
		this._map.setCellType(position, Cell.TYPE_WALL);

		if (direction) {
			this._map.setCellType(position.translate(direction), Cell.TYPE_WALL);

			// move to the neighbouring floor
			let nextDeadEndCandidate = position.translate(direction).translate(direction);
			if (this._isDeadEnd(nextDeadEndCandidate)) {
				this._removeDeadEnd(nextDeadEndCandidate);
			}
		}
	}
}
